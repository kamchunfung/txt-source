「路維克，是要把這邊的書運到書庫嗎？」
「啊，稍等一下，妮可兒，如果要去書庫的話把這卷東西也一起帶上吧。」

路維克手裡抱著大量的箱子和袋子，妮可兒也雙手抱滿了書。在蕾蒂雪爾的屋子裡，傭人們從早上開始就忙碌地工作著。入口處的地板上還放著幾個袋子和箱子，彰顯著它的存在感。
至於為什麼蕾蒂雪爾家門口會出現這麼多東西，這就和今天早上來訪的王的使者有關了。追溯到數小時前，持有国王敕書的使者帶著大貨馬車敲響了蕾蒂雪爾宅邸的門。在奧茲瓦爾德的命令下，使者對前幾天蕾蒂雪爾在魔法省上課一事帶來了謝禮，無奈一輛馬車的份量相當大。裝在馬車裡的是記載了魔法、歷史等各種類型的書籍，對研究有用的捲軸，用於研究的道具，還有裝有大量禮金的袋子。
書籍的量特別多，克勞德也加入進來，從早上開始運送，到了中午終於全部搬運完了。

「不好意思，克勞德。讓你幫忙了。」

這一天，克勞德為了往庭院的池子裡注水，他在黎明前就來了。蕾蒂雪爾所設想的庭院只剩下池塘裡的水了，今天終於迎來了它的完工。儘管如此，從早上開始就讓他幫忙搬行李，蕾蒂雪爾還是覺得有些過意不去。

「不，這不算什麼！比起那個，大小姐，這個箱子放在哪裡好呢？」
「這個啊⋯⋯那請你先搬到一樓最裡面的房間去吧。」

蕾蒂希爾拿著裝有研究工具的箱子，向克勞德發出了指示。收到指示後，克勞德拿著箱子消失在走廊的深處。路維克他們為了前往各自的目的地而離開了大廳，蕾蒂雪爾也為了將手頭的箱子搬到二樓的書齋而邁出了步伐。

（⋯⋯今天一天，這個房間一下子就變得亂七八糟了）

把箱子放在書房的桌子上後，蕾蒂雪爾環視著箱子和袋子隨意擺放著的房內。不僅僅是這個房間，倉庫和圖書室等也僅僅在今天一下子變得擁擠了起來，不過，生活也將因此變得更加充實了。

「哎呀？」

再次回到大廳入口的蕾蒂雪爾，因為看到懷念的身姿而微微地瞪大了眼睛。

「你好，多蘿瑟露姐姐。總覺得你很忙啊。」
「你好，多蘿瑟露姐姐！總覺得好忙啊！」

不知何時，蒂娜和迪特緊緊地牽著手站在變得漂亮的入口中央處。和蕾蒂雪爾同樣回到大廳的三個傭人，因為目睹了只有傳說中才聽過的精靈，驚訝得僵住了。
克勞德睜開眼睛發著呆，妮可兒也驚慌失措地笨拙地動著。至於路維克，或許是因為太過驚訝而勉強地擠出聲音了吧，他捂著嘴，用顫抖的手指指著雙胞胎。

「⋯⋯大小姐⋯⋯他們⋯⋯難道、難道是⋯⋯」
「冷靜下來，路維克，他們就是那個難道是的精靈。」

安慰了語無倫次的路維克後，蕾蒂雪爾看向了雙子。

「事情已經處理好了。你們兩個人為什麼會在這裡呢？」
「那個啊，王城裡的黑霧向南移動了。」
「那個，王城的黑霧經過姐姐家附近，姐姐你沒事吧？」
「黑霧？」
「是啊，從很早以前就一直瀰漫著的漆黑的霧。現在在南方一動不動。」
「是的，是從我們出生開始就在那的漆黑的霧。現在沒動，所以不要緊的。」

雖然不知道什麼黑霧，但從雙胞胎那認真的樣子來看，似乎不是什麼好事。精靈能捕捉到許多人無法感受到的東西。這也是類似的現象吧。

「謝謝你們告訴我。我也會注意的。」
「『嗯！』」
「正好庭院建好了，可以的話我們去玩把？」
「可以嗎？那麼一起玩吧。」
「可以嗎！？那麼一起玩吧！」

雙胞胎手牽著手晃悠悠地一起飛到外面去了。蕾蒂雪爾浮起了苦笑，也追趕著兩人走出了庭院。
蕾蒂雪爾所希望的宅邸庭園，並不是貴族宅邸常見的絢爛豪華的庭園，而是一個有花壇，而且盡可能樸素自然的樣子。蕾蒂雪爾以前世的樹為中心進行投影，在其四周擴展開綠色的草坪，再在其外包圍著五顏六色的秋花。蕾蒂雪爾看著和記憶中風景很相似的庭園，懷念地眯起眼睛笑著。

「兩個人想玩什麼呢？」

在散發著花香的庭院裡，蕾蒂雪爾詢問了雙胞胎。於是兩人一瞬間面面相覷，然後一致指向庭院裡的小池塘。那個池塘可以說是庭院的中心，建在那棵樹的腳下。

「『想玩水！』」
「啊啦，好啊。只是，不要受傷哦。」
「不用擔心。我用魔術來治癒。」
「不用擔心！我會用魔術防禦的！」

仔細想來，蒂娜是光屬性的精靈王，迪特是無屬性的精靈王。應該用不著蕾蒂雪爾來擔心吧。看著在池中歡樂地玩耍的雙胞胎，突然發現屋子那邊妮可兒在盯著這邊看。

（說起來，妮可兒是喜歡精靈的吧）

以前，在被妮可兒打扮的時候，她對自己說過「像精靈一樣美麗」，蕾蒂雪爾則開玩笑地回答道「雖然我沒有長翅膀」，但馬上就被她激烈地反駁道精靈才不是只有翅膀！

「妮可兒，不介意的話也一起來玩吧。」
「誒？我，我、我、我和，精、精、精靈一起玩嗎！？」

蕾蒂雪爾向妮可兒發出了邀請，妮可兒一下子凝固了，以驚人的氣勢搖著頭。

「吶，兩位，能和這個孩子一起玩嗎？」
「大，大小姐！真不好意思！真不好意思！」

妮可兒試圖阻止與雙胞胎精靈王對話的蕾蒂雪爾，但蕾蒂雪爾沒有看漏她眼睛深處那一絲期待的光芒。

「好啊。一起玩吧，侍女的姐姐。」
「好啊！一起玩吧！侍女的姐姐！」
「你看，他們倆都說想一起玩。」

為了回應蕾蒂雪爾的話，坐在樹枝上的蒂娜和迪特看著妮可兒同時跳了下來。妮可兒的心裡，驚訝、困惑還有喜悅交織在了一起，但正因為如此，等她反應過來時，已經被雙胞胎推倒在地了。
微笑地眺望著這樣的情景，蕾蒂雪爾坐在透過樹葉照射下來的陽光下打開了書。其實她從書房回來的時候拿來了一本。前世不能在樹蔭下悠閑地讀書，像這樣一邊乘涼一邊悠閑地讀書，有時也會成為蕾蒂雪爾小小的夢想。雙胞胎把妮可兒拉進來開始捉迷藏。三人臉上浮現出不輸給周圍花朵的鮮艷笑容，蕾蒂雪爾也不由得露出了小小的笑容。
在燦爛的夏日陽光照耀下，一個少女躺在微風吹過的小山丘上，仰望著天空。即使淡淡的金髮沾上泥土她也不嫌棄，少女向著藍天盡情地伸出手。

「⋯⋯啊，在這兒！終於找到你了，蕾蒂！」

山腳下，一個有著漆黑頭髮和眼睛的青年向上跑來。撒著手的少女突然站了起來，將視線投向發出聲音的青年。

「哎呀，納歐。怎麼了？」
「義父大人在叫你哦。關於和傑爾萊德王国邊境線的防衛，他有話想跟你說。」
「⋯」

納歐的話使蕾蒂雪爾的表情陰沉了下來。原以為和傑爾萊德王国構築了良好的關係，但就這樣簡單的被顛覆了嗎？

「⋯⋯這個世界會變得和平嗎？」

在只有兩人的綠色山丘上，蕾蒂雪爾小聲的自言自語道。

「每一天每一天，明明誰都不會幸福，卻總有人在某個地方流血。我想創造一個沒有爭鬥的世界，但那隻能是在夢中嗎⋯？」

對於蕾蒂雪爾這問題的答案，在這個世界上沒有任何人能回答出來。納歐慢慢地閉上眼睛。

「我⋯⋯什麼也說不出來。無論什麼時代，所謂的人就是不得不一直爭鬥下去的生物。」

聽了這番話後，蕾蒂雪爾吃驚地眨著眼睛。

「你的故鄉，比這個世界豐富多彩得多吧？儘管如此也仍然無法停止爭鬥嗎？」
「嗯。在我所在的世界裡，戰爭也並沒有完全消失。」

納歐以認真的表情眺望著在山丘下展開的景色。在眼下展開的城下町裡，是儘管受了傷卻仍在努力地活在當下的人們的身影。

「我想讓戰爭消失一定是很難的事。但是，我想盡可能努力地創造一個大家不爭執、幸福生活的世界。」
「⋯」
「每個人能守護的東西是有限的。所以，首先應該從眼前的人開始一點點地讓他們得到幸福。」

正如納歐所言，在這個看不見戰爭終點的世界裡也許只是一件美好的夢。儘管如此，蕾蒂雪爾還是做著這個夢。

「是啊，正如你所說的。比起在這裡闡述理想，我更應該全力去做我該做的事。」
「嗯，我也會一起加油的。」
「拜託你了，納歐。」

遙遠的夏日回憶，猶如霧靄般無依無靠地徘徊在記憶的海洋中。
不一會兒，納歐開始按原路回去了。蕾蒂雪爾也打算開始追趕他而走了，不經意間，納歐的身姿已經遠去了。
兩人的距離很快就被拉開了，蕾蒂雪爾一邊跑著一邊向他伸出了手。「等一下，不要走，納歐。」蕾蒂雪爾這樣叫著，手卻無法觸碰到納歐。
但是有人抓住了她伸出的手。
眨眼之間，周圍完全變了一個景象。至今為止在那兒的綠色山丘消失了，變成了一片花圃。白色、粉色等淡色的花朵到處盛開著，四周生長著茂盛的樹木。

「看啊，我在這裡！」

手被用力地握著。視野裡閃過的那銀色的頭髮，述說著它的主人是誰。握著小小的多蘿瑟露的手的，是露出了燦爛笑容的克莉絲塔。到底是多少歳呢，大概是三歳吧。

「姐姐大人！這裡就是我最喜歡的地方！」
「⋯⋯真的是，漂亮的地方。」

和蕾蒂雪爾的意識無關，嘴巴自己就動了。克莉絲塔那驕傲的笑容閃閃地發光。

「我們一起飛進花海裡吧，很快樂的喲！姐姐也一起來吧！」

克莉絲塔稍微助跑了一下後，就飛進了花圃。淡淡的花瓣輕輕地飛舞著，溫暖的風包圍著它們，並將花瓣卷向天空。
克莉絲塔暫且就那樣在花圃中打著滾兒，但是多蘿瑟露拿出了雪白的花冠。克莉絲塔看到那個白色的花圈時，一瞬間停止了動作，莫名其妙地站了起來。

「哇⋯⋯！真漂亮！姐姐，這是怎麼弄的？」
「就算問我⋯」

沒有多說什麼，多蘿瑟露將白色的花冠塞到克莉絲塔的手上。

「難道說，這是給我的？」
「嗯，給你了。」

二人周圍飛舞著被花香味吸引的蝴蝶，在這樣的夢幻般的場景中，克莉絲塔戰戰兢兢地從多蘿瑟露那兒接過了花冠。

「好厲害⋯⋯好可愛！謝謝你！姐姐大人！」
「如果喜歡的話，還會再給你做的。」
「那麼請告訴我做法！」

面對著天真無邪地歡鬧的克莉絲塔，多蘿瑟露輕輕地點了點頭。
涼爽的秋風吹拂著臉頰，蕾蒂雪爾的意識慢慢清醒了過來。視線向下看，膝蓋上就放著打開著的書。她好像是邊看書邊睡著了。背靠在樹榦上，蕾蒂雪爾望向頭上的綠蔭。也許是因為秋意漸濃，一陣風吹過時，褪色的葉子像滑行一樣飄落了下來。

（⋯⋯為什麼做了這樣的夢呢⋯）

到現在為止，蕾蒂雪爾雖然是作為多蘿瑟露活著，但是卻沒有多蘿瑟露的記憶。但是最近，同一個身體裡有兩個不同的意識的傾向一點點地變強了。現在的夢也微妙地帶有現實感，好像多蘿瑟露的記憶進入了蕾蒂雪爾的記憶之中了，為一瞬間不明白自己是誰的這種感覺感到了害怕。
朝著花圃的方向看去，那裡有躺在花海裡的迪特，和正在努力製作花冠的蒂娜。沒有看到妮可兒的身影，也許是回到宅邸裡了。
轉動視線，蕾蒂雪爾看向著王都的方向⋯⋯將她那桃色和藍色的瞳孔轉向克莉絲塔的方向。克莉絲塔似乎非常憎恨多蘿瑟露，不過兩人似乎也有關係很好的時期。
作為多蘿瑟露轉生已經半年多了，蕾蒂雪爾第一次對克莉絲塔這個雙胞胎妹妹的存在表現出了興趣。這是夢呢，還是多蘿瑟露的記憶呢？如果是記憶的話⋯⋯⋯

（克莉絲塔為什麼會恨多蘿瑟露到這種地步呢？）

對於蕾蒂雪爾心中浮現出的疑問，秋風沉默地飛奔在寂靜的庭院中。