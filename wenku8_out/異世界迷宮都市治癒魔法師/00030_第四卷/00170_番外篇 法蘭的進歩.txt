關於莎丘芭絲即將來襲的對策會議開完後過了一天。當我在領主分配給我的客房裡，讓悠艾兒坐在大腿上撫摸著她的頭時，房間的門突然打了開來。

「我聽說了哦，你其實是有魔力的嘛。」

才剛看到法蘭走進房間裡，她就接著吐出了這句話。

從法蘭的口氣能感受得到確實的怒意，她的表情也像是鼓起了腮幫子。

「突然闖了進來，真是抱歉。」

仔細一看，莎拉也一起來了。她跟著法蘭進入房間裡後，立刻朝著我低頭道歉。

「都是因為你不說實話，才讓我在教你魔法時，以為你有魔力漏出症⋯⋯為什麼要瞞著我啊！」

法蘭的視線瞄過悠艾兒後，又回到了我的身上，並以和平時一樣的態度大聲叫道。

「啊──你說那個啊⋯⋯」

⋯⋯這麼說來，法蘭以前曾教過我一點攻擊魔法。

我記得那個時候得出的結論是，由於我會漏出大量的魔力，使得能夠使用的魔力量很少，而無法使用攻擊魔法。

當時法蘭曾說只要經過訓練就有可能治癒，還給了我測量自己周圍魔力濃度的魔道具。

我不想讓自己的實力曝光才瞞著她，不過從法蘭那時的感覺來看，她好像還滿同情我的。

⋯⋯她會生氣是理所當然的。

但我也並非毫無意義地瞞著她，這是有正當理由的。

「真不好意思，可是我並非毫無意義地瞞著你。若是沒有必要，之前我都是盡可能地不讓別人知道自己的實力。」
「那有什麼關係，被人知道實力又不會怎樣！」

法蘭更加發怒，把臉湊了過來。看來她真的是火冒三丈了。

距離好近。

臉好近。

⋯⋯這下不妙啦。

她一生氣，可是會動手的。

不對，與其說動手，應該說動魔法。

而且由於她多少有些實力，雖然不甚安定，也還是能以無咏唱的方式施展魔法。

悠艾兒現在坐在我大腿上，法蘭應該是不致於在這種狀況下朝我擊出魔法，但我姑且還是將力道聚集在下半身，以便隨時能夠跳到旁邊去。

接著我為了解釋而正要開口時：

「法蘭，我聽說四季先生這次會被牽連進大主教的陰謀，契機就在於他的究極療癒⋯⋯你也看到四季先生那時的樣子了吧？」

莎拉就先一步如此說道。

「這、這個⋯⋯」

法蘭頓時語塞。

也是，在來到領主宅邸借用浴場之前，我處於渾身是血的狀態。

回想起來，在到達領主宅邸時，法蘭好像就是一臉訝異地看著我。

她大概是想起那時的狀況了吧。

「⋯⋯可、可是，父親已經和我說了唷，在旋殻大海龜事件時所發生的神跡也是你做的，對吧？不想引人注目卻又做出那種事情，這、這不會很矛盾嗎？」

但即使如此，法蘭還是不肯罷休。

她大概是不曉得要將氣出在哪裡吧。

⋯⋯這或許就代表她當時要教我魔法時，就是這麼地認真。

然而，我無法誠實地回答這個問題。悠艾兒在這裡，她正乖乖地坐在我的大腿上讓我撫摸她的頭。

「那、那個啊⋯⋯就是，沒錯⋯⋯是為了城鎮的居民著想啦。」

其實只是因為當時悠艾兒快要哭出來了，我為了安慰她並轉移她的注意力，才向她這麼說的。

但悠艾兒直到現在還因為自己沒能保護我而沮喪失意，可不能在她的面前說出當時那件事只是為了安慰她才那麼做的。

「⋯⋯為、為了城鎮的居民著想？你、你、你是說為了這座梅爾海茲鎮著想⋯⋯？意思是說你為了拯救這座城鎮於危機之中，而背負了那麼大的風險⋯⋯？」

不過，法蘭卻比我想像的還要吃驚。

⋯⋯這麼說來，法蘭是領主的女兒，可以說是下一代的領主。

即使我沒這個意思，但好像讓她覺得我對她有恩了。

法蘭就保持著逼近我的姿勢僵在原地，看著我的臉。

「太、太近了啦！」

接著她立刻臉紅起來，與我拉開距離。

⋯⋯自己逼近過來還對我抱怨「太近了！」真是有夠不講理。

「⋯⋯我要回房間了。」

於是法蘭就顯露出有些泄氣的樣子，就這樣背向我走出房間去。

「呵呵，打擾您了，四季先生。」

莎拉向我行過一禮後，也離開了房間。她的臉色看起來有點高興。

⋯⋯終於出去了。

我將聚在腳上的力量放鬆。法蘭還在房間裡時，我一直提神戒備，讓自己隨時都能往旁邊跳。

這樣子防備她可能是有點過度了，但那傢伙可有將攻擊魔法轟向相親對象胯下的前科。縱使是未逐，她也曾企圖對我放出攻擊魔法。

⋯⋯我姑且還是知道法蘭並不是什麼壊人就是了。

不過這種類型的人就是不知何時會踩到她的地雷，然後爆炸。

這下總算能放輕鬆了。

「法蘭？⋯⋯你在這裡做什麼？」
「父、父親⋯⋯」

然而房間外頭卻傳出了聲音。是領主與法蘭的說話聲。

看來是法蘭走出房間時，碰巧撞見了領主。

「莫、莫非法蘭你，有、有考慮那件事了嗎⋯⋯？」
「唔⋯⋯」
「法、法蘭！」

接著傳進來的，是從房間前快步離去的腳步聲。

領主被女兒忽視的傷心聲音傳到了耳裡。

⋯⋯看來法蘭正處於難以捉摸的年紀，雖然我不曉得他們兩人在說什麼。

⋯⋯不過領主是來這裡做什麼的呀？

這間房間位於走廊的盡頭，總不會是偶然經過吧。

又要開莎丘芭絲的對策會議了嗎？

就在我這麼想時，房門的敲門聲就響了起來。

「突然打擾，真是不好意思。」
「⋯⋯不會。」

領主一進入房間後，就坐在旁邊的椅子上。

他的臉色不太好看，大概是為了研擬莎丘芭絲來襲的對策之類的事，而繁忙勞碌吧。

「那麼⋯⋯你和法蘭說了些什麼？」

領主看著我如此間道，同時將身體稍微往前傾。

他的表情好像是在期待著什麼。

⋯⋯一個大叔露出這種表情給我看，也沒什麼好高興的。

「是關於我在治癒魔法上的實力啦。」

本來不知該怎麼回答，不過也不用答得太詳細吧。

從他特地來到這間房間來看，主題八成是別的事情。

再說我正忙著撫摸悠艾兒的頭，雖說不至於沒空和這個大叔講話，但可也沒太多時間陪他。

「⋯⋯這樣啊。四季，不，是四季殿下，你覺得法蘭如何呢？」

然而領主卻繼續說著法蘭的話題。

⋯⋯這麼說來，這個領主以前曾向我說過「以後我女兒就交給你了」之類的話，莫非他來到這間房間的目的是⋯

我提神戒備，並把話接下去。

「你的意思是？」
「哎啊，從我這個當父親的人來看，也認為法蘭的長相還滿標緻的。我聽說四季殿下你還未婚，是否對法蘭抱持著特別的感情──」
「並沒有。」
「哎、哎呀，我並不是要你立刻作出結論，只要你們在一起生活，你一定會改變心──」
「我已經有其他的人選了。」

我答得如此果斷，讓領主本來就很蒼白的臉色變得更加慘白了。

⋯⋯這位領主大叔也很努力呢。

就連得忙著提防莎丘芭絲的這種時候，都還在為女兒找結婚對象，看來他真的是很想盡遠確保女兒的適婚人選。

雖然開始可怜起他來，但我真的另有人選了。

露露卡已經說過喜歡我，再加上昨天我在清洗身上血跡時愛麗絲來到了浴場，她當時的反應也讓我很在意。

感覺沒有必要硬要向法蘭出手，何況她又沒有胸部。

「不、不過呢，法蘭可是有很多優點的哦，就、就是啊，她的長相很標緻⋯⋯還、還有，對了，很苗條，她很苗條。」

⋯⋯你就只有誇她的外表而已耶，領主先生。

他大概是想不到我會這麼快就表態，而焦急起來了吧。

然而卻有個意料之外的人物，為領主幫腔了。

「法蘭小姐教了我許多我所不知道的歷史，而且還和我約好下次要教我攻擊魔法。法蘭小姐⋯⋯是位溫柔的人。」

是悠艾兒，坐在我大腿上的悠艾兒如此呢喃道。

⋯⋯這麼說來，法蘭在我洗澡時幫忙照顧了悠艾兒。

再從她教我魔法時的態度來看，她或許意外地會照顧人。

但完全沒有胸部以及超級急性子這兩點，將一切都糟蹋掉了。

「就、就是這樣！法蘭意外地會照顧人呢。縱使很容易白忙一場，但她想要守護這座城鎮的責任感比其他人還強得多。法蘭只是因為有些直腸子而老是直接動手，但她其實是個非常溫柔的孩子。」

領主開始向我說起法蘭的好話了。

⋯⋯她差點用火球術砸我耶，哪只是「動手」的程度而已。

話說回來，法蘭十分討厭男人，就算領主再怎麼勸婚，她也不會認同的吧。

「我說真的，你能不能就娶了她呢⋯⋯對象若是四季殿下，我認為這樁婚事一定會很圓滿順利。」

領主以懇求的語氣如此低語道。

⋯⋯法蘭的長相不難看，而且攻擊魔法也很優秀，身為領主的女兒，也有身分地位。

總不至於完全找不到一個對象吧。

站在領主的角度來看，由於我能使出超乎常人的治癒魔法，他可能多少將我視為優異人選，可是還是希望他能去找別人。

「再這樣下去，真的就沒人要娶她了⋯⋯她那種性格要是錯過了適婚年齡，我就不知道該怎麼辦了⋯⋯」

領主垂頭喪氣的模樣，讓他看起來縮小了一圈。

領主說完「希望之後能再和你談談這件事」這句話後就離開了房間。

過了十幾分鐘，房間的門再度被打開了。

「剛剛真是抱歉。」

我還以為領主才走掉沒多久卻又馬上過來，結果來到房間的人是法蘭與莎拉。

法蘭大步大步地走進房間，站在我的眼前。

接著她稍微移開視線，開口說道：

「你不僅是救了我的命，連應該會由我來治理的城鎮都拯救了，既然如此⋯⋯呃，我也不會太吝嗇的。」

法蘭感覺有些坐立不安地說出了這樣的話。

但我不太懂她的話中涵意。

我也不會太吝嗇？

這是什麼意思？

⋯⋯不，從剛才領主所說的話來想：

「莫非，你是說結婚嗎？」
「才、才不是呢！你、你怎麼會扯到那裡去！？」

不是啊。

也對，從法蘭的性格來看，這是不可能的。

那不然會是什麼意思呢？

「就、就是啊⋯⋯要是又有什麼事情，你可以來拜託我。我是這個意思啦。」

法蘭的臉朝著下方，如此補充道。

「哎呀！」

莎拉把手放在嘴邊，發出非常高興的聲音。

「不、不過呢，這只是因為我身為領主的女兒，有人拯救了城鎮卻不回禮，會讓我心裡覺得不舒服而已！真、真的就只是這樣哦！」

法蘭對莎拉的聲音做出反應，開始慌張起來。

我隱約可以明白。

⋯⋯這大概是法蘭對於我拯救了城鎮，感到受恩於人，而盡力做出的回報吧。

不過，感覺她會白忙一場。

法蘭太過直腸子，不管拜託她什麼事情，感覺都會變成麻煩的狀況。

但是呢，我也沒別扭到會無視法蘭這番努力說出的發言。

「哦，我會考慮的。」

我一如此答道，法蘭就把手放在胸口上，像是鬆了一口氣。

她大概是對於我是否會答應而感到不安吧，如果會這麼不安，就無視這個人情不就好了。

⋯⋯在她教我魔法時就這麼覺得了，法蘭真的很重視人情義理呢。

要是她的個性不急躁，想必會有很多人要娶她的說。

就在我這麼想時，莎拉突然靠近了過來。

「也請你考慮一下和法蘭的婚事吧。」
「這可沒得談。」

可能是由於剛才和領主說過了類似的話，我反射性地即時答道。

但在回答之後，我才發覺到一件事。

⋯⋯法蘭現在就在我的眼前。

糟了。

「～～唔！？」

無論如何，即時否定絶對會讓事情不妙。

法蘭好歹也是位女性，就算她對我沒有意思，也還是有自尊的。

法蘭的臉變得愈來愈紅。

這樣的臉紅肯定不是起因於害羞之類的感情。

⋯⋯是憤怒。

光是瞧臉色，就可以肉眼確認法蘭的憤怒正在直線上升。

「本、本小、本小姐才要向你說聲敬謝不敏呢！！！」

接著法蘭的臉旁溢出了紅光。

無咏唱的火球術，就這樣砸在了石造地板上。