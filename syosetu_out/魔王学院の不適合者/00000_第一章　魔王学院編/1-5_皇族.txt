「傑貝斯・印德承認投降。勝利者是阿諾斯・波魯迪戈烏多。」

伴隨貓頭鷹的宣言，出入口的魔法屏障解除了。
然而讓人感到不可思議的是，儘管我報上了魔王阿諾斯之名，觀眾席上的人們卻一副彷彿從未聽聞的反應。
該不會是假冒魔王之名的不肖之徒太多，事到如今大家已經不以為意了吧？
變得太有名也很傷腦筋呢，但只要用實力證明就好了吧。

「是場相當不錯的比賽呢。」

為了讚揚彼此的善戰，我向傑貝斯伸出手。
他卻像是嚇到似的，身體抖了一下。

「混、混賬東西！竟敢瞧不起我！給我記住！」

拋下很有小癟三風格的狠話後，傑貝斯逃了回去。
唔，比賽結束後明明就無冤無仇，那傢伙為什麼會這麼生氣啊？
儘管他確實是因為我而不合格，但我也沒有取他性命，下次再來參加測試不就好了？
倒不如說他在和我戰鬥後可是還能好手好腳的回去，若是神話時代的魔族，應該會是足以讓人感動落淚的無上幸福吧。

「休息十分鐘後，再請你與下一位測驗生進行決鬥。」
「不需要。」

這種連暖身運動都不算的決鬥，每打一場就要休息十分鐘，即使是我也會閒到發慌。
畢竟之後還得跟四個人打，我想祈禱至少別是無名小卒。

「基於阿諾斯・波魯迪戈烏多的要求，休息省略。」

此時，在傑貝斯逃回去的通道方向上，我望見了進發的魔力流動。

「呀啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊！」

慘叫聲響起。接著便見出入口出現了一名長髮魔族。
他眉間緊皺，有著一張感覺很神經質的長相。那名魔族隻手抓住傑貝斯的脖子，將他舉了起來。

「大、大哥⋯⋯是、是我不好⋯⋯原諒我吧。我下次一定⋯⋯」
「不知羞恥。」

長髮魔族捏碎傑貝斯的喉嚨，並在手上聚集起魔力粒子。漆黑雷電劈啪作響，傑貝斯頓時全身燒焦。

「呀啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊！」

轉眼間，傑貝斯便化為焦炭。
長髮魔族像是丟棄垃圾似的拋開他後，朝我走了過來。

「我的弟弟承蒙你關照了。」

原來如此，這傢伙是傑貝斯的哥哥啊。看樣子是比弟弟正經些，卻讓人看不順眼呢。

「倘若是來替弟弟報仇，這倒會是句好臺詞呢。」
「竟然輸給區區的雜種，真是我一族的恥辱。親手給他一個痛快是我最起碼的憐憫。」

雜種是在說我嗎？
雖然我不會對這種事情一一生氣啦，但我要是雜種，你自己豈不是成了雜種的子孫了，這樣好嗎？這就像是兄弟在吵架時，大罵「你媽媽是醜八怪」一樣滑稽呢。

「我認為兄弟就該互助合作吧。」
「天真的男人。擁有力量才算得上是魔王族。」

哎呀哎呀，到底是誰天真啊？縱使再怎麼弱小，這種胡來的殺害依舊毫無意義，弱小有弱小的用處。犯下無謂減少夥伴的愚行，在神話時代會活不下去吧。

「看來你對力量好像有所誤解呢。」
「你還真是愛開玩笑。明明殺掉他就結束了，你卻特地用上『復活』這種大魔法，只為了讓他開口投降。」

男人說得像是他親眼目睹似的，讓我在意起來，朝觀眾席看去。
原來如此，是那裡嗎？在第三排的位置上有一群沒穿制服的魔族，恐怕是測驗生吧。也就是說，他們能觀看之後比賽對手的戰鬥嗎？
但我無法理解。我所排的Ｆ隊伍是筆直通往這座競技場，直接開始實戰測驗，應該沒有機會能到觀眾席觀戰。

「看樣子你似乎不知道呢。我們純血的魔王族──也就是皇族──能挑選比賽對手，待遇跟你們這種只是混到始祖之血的雜種不同。」

哦，也就是說邀請函上記載的字母，是以混到我的血的程度來區分嗎？儘管不知道是誰想的主意，但還真是滑稽。
無論純血、混血，都與繼承到我多少力量無關。畢竟要是這麼容易分辨，簡直就像在說「請在轉生前殺了我」一樣。
說到底，「因為純血所以強大；因為混血所以弱小」的理論也缺乏根據。魔王之血只要一滴就夠了。
就連這種事也想不到，老實說還真是讓我傻眼。

「看來你總算理解自己的立場了吧。」
「不，在說什麼呢？我只是覺得真虧你們會在意這種無聊的事情。」

男人的額角抽動了一下。

「⋯⋯無聊的事？」
「你想想看，所謂魔王只是因為強大而被擅自這樣稱呼的。魔王談起純血？立場？哈！別笑死人了。」

我的嘲笑讓長髮魔族怫然變色。他說不定以純血為榮，但這不過是無聊的自尊心。

「我是不介意你們建立特權階級啦，無論任何時代都會出現這種傢伙。但所謂的魔王是憑藉一己之力，制伏一切權力與法規者。想不到如今他的子孫居然會是擁有特權的一方啊。」

看來是被我這番瞧不起人的說法觸怒了吧，長髮魔族投來殺氣騰騰的視線。

「我認定你剛才的發言是輕視我等始祖的偉業，批評皇族的言論。既然如此，本人魔大帝──里歐魯格・印德，就當親手對你處以死刑。」
「我談論自己，為什麼會是輕視自己的偉業？」
「⋯⋯什麼？」
「真是遲鈍的傢伙。意思是說，我就是你們的始祖。」

聞言，里歐魯格露出明顯充滿憎惡的眼神，朝我瞪了過來。

「你這傢伙知道自己在說什麼嗎？」
「說什麼？是指我就是我自己的事嗎？」

里歐魯格忍無可忍似的大叫起來：

「你那假冒自己是始祖的不敬態度，實在罪該萬死！」
「真搞不懂呢。照你的想法，轉生後的魔王難道會是個連自己是誰都不知道的蠢蛋嗎？」
「閉嘴！蠢的是你這傢伙，竟敢質疑七魔皇老的直言，這可是大罪哦！」

唔，七魔皇老嗎？又跑出一個莫名其妙的外號，之後再調查吧。

「你所說的話完全缺乏根據。算了，我不怪你，畢竟魔王不是能用話語證明的嘛。」
「你這傢伙！又在嘲弄七魔皇老的直言嗎？」

我沒有這個意思就是了，這傢伙還真是麻煩呢。

「要打就快打吧。我會讓你親身體會我就是始祖這件事。」

我還以為只要挑釁，他就會立刻衝過來。出乎意料的是，那傢伙卻朝無關緊要的方向看去──是觀眾席。

「就讓這傢伙知道批評皇族的人會有怎樣的下場吧。」

里歐魯格話一說完，觀眾席上的三名魔族便跳進競技場。

「唔，這樣好嗎？我記得現在應該是入學測驗吧？」

聽我這麼一問，里歐魯格理所當然似的說道：

「你在怕什麼？這無庸置疑是正式的入學測驗哦。要一一打倒很麻煩對吧？所以這只是在幫你省工夫。既然要證明自己是始祖，這點讓步是當然的事。」

飛在上空的貓頭鷹應該是裁判吧，看上去卻似乎不打算指出他犯規。
原來如此，這也是特權嗎？當皇族的力量不足時，就會靠這種手段讓他們合格吧。

「不過四個人是怎麼回事？」
「現在才想道歉也來不及了。你就後悔自己的發言，然後去死吧。」
「你誤會了什麼？我是說太少了。」

里歐魯格的表情更顯凶惡。

「你說什麼？」
「要證明我是始祖，四個無名小卒是不夠的。好啦，聚在那裡看戲的傢伙通通一起上吧。」
「你這傢伙⋯⋯」

不待里歐魯格指示，在觀眾席上觀戰的魔族們陸陸續續地跳到競技場上，是皇族什麼的吧？各個都朝我露出了不服的表情。
看上去約有八十人。

「俗話說得好，這就叫禍從口出。」
「就是說啊。假如你沒說多餘的話，就沒必要出現八十人之多的犧牲了呢。」

里歐魯格看似不悅地蹙起眉頭，隨即卻像是改變主意般笑了起來。

「就算是假冒始祖的不肖之徒，單方面的虐殺也有損皇族之名。我給你十秒，十秒內我們不會出手，你就趁這段時間儘可能準備強力的魔法吧。」
「哦⋯⋯夥伴一增加，口氣就突然變得這麼大啊。真是下流的傢伙。」

或許是人多勢眾的從容吧，直到剛剛仍頻頻暴怒的男人忽然笑容滿面。

「還有空講話嗎？已經過了十秒嘍。」

聽到里歐魯格自以為勝利的得意臺詞，我忍不住笑了起來。

「咯咯咯⋯⋯哈哈哈！」
「有什麼好笑的？你是太過恐懼到腦袋不正常了嗎？」
「還沒注意到嗎？更加睜大魔眼，仔細瞧瞧吧。」

魔眼是指觀看魔力的視力。聽到警告，里歐魯格這才將魔力注入眼中，為偵測魔力的流動而啟動魔眼，隨即大吃一驚。
看來他總算注意到自己的魔力正逐漸失控了呢。
團團圍住我的魔族們慘叫般的喊道：

「這、這是怎麼回事⋯⋯？魔力不聽控制⋯⋯！」
「怎麼可能⋯⋯就連展開魔法陣的動作也沒有⋯⋯這種事⋯⋯快住手⋯⋯！」
「這傢伙⋯⋯對多達八十人的皇族⋯⋯同時施展魔法⋯⋯！」
「他、他做了什麼？他到底對我們做了什麼啊！」

哎呀，才這樣就受不了，真不像樣啊。

「好啦，趕快控制住自己的魔力吧。不然──」

將我團團包圍的魔族們臉色發白，想方設法控制自己漸漸失控的魔力。然而為時已晚。

「會死哦。」

瞬間，伴隨刺耳聲響，跳到競技場上的八十人就像是被引爆的火藥庫般，盛大地爆炸了。